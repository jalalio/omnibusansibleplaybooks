---
# defaults file for php-fpm_high_performance_configuration
php:
  php_version: '7.3'
  ########################################################
  ### Service Configuration 
  ### /etc/php/{{ php.php_version }}/fpm/php-fpm.conf
  ########################################################
  # syslog_facility is used to specify what type of program is logging the
  # ; message. This lets syslogd specify that messages from different facilities
  # ; will be handled differently.
  # ; See syslog(3) for possible values (ex daemon equiv LOG_DAEMON)
  # ; Default Value: daemon
  syslog_facility: 'daemon'
  # syslog_ident is prepended to every message. If you have multiple FPM
  # ; instances running on the same server, you can change the default value
  # ; which must suit common needs.
  # ; Default Value: php-fpm
  syslog.ident: 'php-fpm'
  # ; Log level
  # ; Possible Values: alert, error, warning, notice, debug
  # ; Default Value: notice
  log_level: 'notice'
  # ; Log limit on number of characters in the single line (log entry). If the
  # ; line is over the limit, it is wrapped on multiple lines. The limit is for
  # ; all logged characters including message prefix and suffix if present. However
  # ; the new line character does not count into it as it is present only when
  # ; logging to a file descriptor. It means the new line character is not present
  # ; when logging to syslog.
  # ; Default Value: 1024
  #### >= 7.3
  log_limit: '4096'
  # ; Log buffering specifies if the log line is buffered which means that the
  # ; line is written in a single write operation. If the value is false, then the
  # ; data is written directly into the file descriptor. It is an experimental
  # ; option that can potentionaly improve logging performance and memory usage
  # ; for some heavy logging scenarios. This option is ignored if logging to syslog
  # ; as it has to be always buffered.
  # ; Default value: yes
  #### >= 7.3
  log_buffering: 'no'
  # ; If this number of child processes exit with SIGSEGV or SIGBUS within the time
  # ; interval set by emergency_restart_interval then FPM will restart. A value
  # ; of '0' means 'Off'.
  # ; Default Value: 0
  emergency_restart_threshold: '10'
  # ; Interval of time used by emergency_restart_interval to determine when
  # ; a graceful restart will be initiated.  This can be useful to work around
  # ; accidental corruptions in an accelerator's shared memory.
  # ; Available Units: s(econds), m(inutes), h(ours), or d(ays)
  # ; Default Unit: seconds
  # ; Default Value: 0
  emergency_restart_interval: '1m'
  # ; Time limit for child processes to wait for a reaction on signals from master.
  # ; Available units: s(econds), m(inutes), h(ours), or d(ays)
  # ; Default Unit: seconds
  # ; Default Value: 0
  ### Comment:
  # @see https://geekflare.com/php-fpm-optimization/
  # The previous two settings are cautionary and are telling the php-fpm process 
  # that if ten child processes fail within a minute, the main php-fpm process 
  # should restart itself. This might not sound robust, but PHP is a short-lived 
  # process that does leak memory, so restarting the main process in cases of 
  # high failure can solve a lot of problems.
  process_control_timeout: '10s'
  # ; The maximum number of processes FPM will fork. This has been designed to control
  # ; the global number of processes when using dynamic PM within a lot of pools.
  # ; Use it with caution.
  # ; Note: A value of 0 indicates no limit
  # ; Default Value: 0
  ### Comment:
  # @see https://geekflare.com/php-fpm-optimization/
  # The third option, process_control_timeout, tells the child processes to wait 
  # for this much time before executing the signal received from the parent process. 
  # This is useful in cases where the child processes are in the middle of something 
  # when the parent processes send a KILL signal, for example. With ten seconds on 
  # hand, they’ll have a better chance of finishing their tasks and exiting gracefully.
  process_max: '128'
  # ; Specify the nice(2) priority to apply to the master process (only if set)
  # ; The value can vary from -19 (highest priority) to 20 (lowest priority)
  # ; Note: - It will only work if the FPM master process is launched as root
  # ;       - The pool process will inherit the master process priority
  # ;         unless specified otherwise
  # ; Default Value: no set
  process_priority: ''
  # ; Send FPM to background. Set to 'no' to keep FPM in foreground for debugging.
  # ; Default Value: yes
  daemonize: yes
  # ; Set max core size rlimit for the master process.
  # ; Possible Values: 'unlimited' or an integer greater or equal to 0
  # ; Default Value: system defined value
  ### See the pool rlimit_core settings below
  service_rlimit_core: 'unlimited'
  # ; Specify the event mechanism FPM will use. The following is available:
  # ; - select     (any POSIX os)
  # ; - poll       (any POSIX os)
  # ; - epoll      (linux >= 2.5.44)
  # ; - kqueue     (FreeBSD >= 4.1, OpenBSD >= 2.9, NetBSD >= 2.0)
  # ; - /dev/poll  (Solaris >= 7)
  # ; - port       (Solaris >= 10)
  # ; Default Value: not set (auto detection)
  events_mechanism: ''
  # ; When FPM is built with systemd integration, specify the interval,
  # ; in seconds, between health report notification to systemd.
  # ; Set to 0 to disable.
  # ; Available Units: s(econds), m(inutes), h(ours)
  # ; Default Unit: seconds
  # ; Default value: 10
  systemd_interval: '10'
  ########################################################
  ### Pool Configuration
  ### /etc/php/{{ php.php_version }}/fpm/pool.d/{{ php.pool_name }}.conf
  ########################################################
  # Start a new pool named 'www'.
  # the variable $pool can be used in any directive and will be replaced by the
  # pool name
  pool_name: 'www'
  # Unix user/group of processes
  # ; Note: The user is mandatory. If the group is not set, the default user's group
  # ;       will be used.
  user: 'www-data'
  group: 'www-data'
  # The address on which to accept FastCGI requests.
  listen: '/run/php/php7.3-fpm.sock'
  #  Set listen(2) backlog. Default Value: 511
  listen_backlog: '511'
  # Set permissions for unix socket, if one is used. In Linux, read/write
  # permissions must be set in order to allow connections from a web server. 
  listen_owner: 'www-data'
  listen_group: 'www-data'
  listen_mode: '0660'
  # ; List of addresses (IPv4/IPv6) of FastCGI clients which are allowed to connect.
  # ; Equivalent to the FCGI_WEB_SERVER_ADDRS environment variable in the original
  # ; PHP FCGI (5.2.2+). Makes sense only with a tcp listening socket. Each address
  # ; must be separated by a comma. If this value is left blank, connections will be
  # ; accepted from any ip address.
  # ; Default Value: any
  listen_allowed_clients: 'any'
  # Choose how the process manager will control the number of child processes.
  # ; Possible Values:
  # ;   static  - a fixed number (pm.max_children) of child processes;
  # ;   dynamic - the number of child processes are set dynamically based on the
  # ;             following directives. With this process management, there will be
  # ;             always at least 1 children.
  # ;             pm.max_children      - the maximum number of children that can
  # ;                                    be alive at the same time.
  # ;             pm.start_servers     - the number of children created on startup.
  # ;             pm.min_spare_servers - the minimum number of children in 'idle'
  # ;                                    state (waiting to process). If the number
  # ;                                    of 'idle' processes is less than this
  # ;                                    number then some children will be created.
  # ;             pm.max_spare_servers - the maximum number of children in 'idle'
  # ;                                    state (waiting to process). If the number
  # ;                                    of 'idle' processes is greater than this
  # ;                                    number then some children will be killed.
  # ;  ondemand - no children are created at startup. Children will be forked when
  # ;             new requests will connect. The following parameter are used:
  # ;             pm.max_children           - the maximum number of children that
  # ;                                         can be alive at the same time.
  # ;             pm.process_idle_timeout   - The number of seconds after which
  # ;                                         an idle process will be killed.
  # ; Note: This value is mandatory.
  pm: 'dynamic'
  # The number of child processes to be created when pm is set to 'static' and the
  # ; maximum number of child processes when pm is set to 'dynamic' or 'ondemand'.
  # ; This value sets the limit on the number of simultaneous requests that will be
  # ; served. Equivalent to the ApacheMaxClients directive with mpm_prefork.
  # ; Equivalent to the PHP_FCGI_CHILDREN environment variable in the original PHP
  # ; CGI. The below defaults are based on a server without much resources. Don't
  # ; forget to tweak pm.* to fit your needs.
  # ; Note: Used when pm is set to 'static', 'dynamic' or 'ondemand'
  # ; Note: This value is mandatory.
  pm_max_children: '5'
  #  The number of child processes created on startup.
  # ; Note: Used only when pm is set to 'dynamic'
  # ; Default Value: min_spare_servers + (max_spare_servers - min_spare_servers) / 2
  pm_start_servers: '2'
  # The desired minimum number of idle server processes.
  # ; Note: Used only when pm is set to 'dynamic'
  # ; Note: Mandatory when pm is set to 'dynamic'
  pm_min_spare_servers: '1'
  # The desired maximum number of idle server processes.
  # ; Note: Used only when pm is set to 'dynamic'
  # ; Note: Mandatory when pm is set to 'dynamic'
  pm_max_spare_servers: '3'
  # ; The number of seconds after which an idle process will be killed.
  # ; Note: Used only when pm is set to 'ondemand'
  # ; Default Value: 10s
  pm_process_idle_timeout: '10s'
  # The number of requests each child process should execute before respawning.
  # ; This can be useful to work around memory leaks in 3rd party libraries. For
  # ; endless request processing specify '0'. Equivalent to PHP_FCGI_MAX_REQUESTS.
  # ; Default Value: 0
  pm_max_requests: '500'
  # The URI to view the FPM status page. If this value is not set, no URI will be
  # ; recognized as a status page.
  # ; By default the status page output is formatted as text/plain. Passing either
  # ; 'html', 'xml' or 'json' in the query string will return the corresponding
  # ; output syntax. Example:
  # ;   http://www.foo.bar/status
  # ;   http://www.foo.bar/status?json
  # ;   http://www.foo.bar/status?html
  # ;   http://www.foo.bar/status?xml
  # ;
  # ; By default the status page only outputs short status. Passing 'full' in the
  # ; query string will also return status for each pool process.
  # ; Example:
  # ;   http://www.foo.bar/status?full
  # ;   http://www.foo.bar/status?json&full
  # ;   http://www.foo.bar/status?html&full
  # ;   http://www.foo.bar/status?xml&full
  status_path: '/status'
  # ; The ping URI to call the monitoring page of FPM. If this value is not set, no
  # ; URI will be recognized as a ping page. 
  ping_path: 'ping'
  # This directive may be used to customize the response of a ping request. The
  # ; response is formatted as text/plain with a 200 response code.
  # ; Default Value: pong
  ping_response: 'pong'
  # The log file for slow requests
  # ; Default Value: not set
  # ; Note: slowlog is mandatory if request_slowlog_timeout is set
  slowlog_path: '/var/log/$poll.log.slow'
  # The timeout for serving a single request after which a PHP backtrace will be
  # ; dumped to the 'slowlog' file. A value of '0s' means 'off'.
  # ; Available units: s(econds)(default), m(inutes), h(ours), or d(ays)
  # ; Default Value: 0
  request_slowlog_timeout: '0'
  # ; Depth of slow log stack trace.
  # ; Default Value: 20
  request_slowlog_trace_depth: '20'
  # The timeout for serving a single request after which the worker process will
  # ; be killed. This option should be used when the 'max_execution_time' ini option
  # ; does not stop script execution for some reason. A value of '0' means 'off'.
  # ; Available units: s(econds)(default), m(inutes), h(ours), or d(ays)
  # ; Default Value: 0
  request_terminate_timeout: '0'
  # The timeout set by 'request_terminate_timeout' ini option is not engaged after
  # ; application calls 'fastcgi_finish_request' or when application has finished and
  # ; shutdown functions are being called (registered via register_shutdown_function).
  # ; This option will enable timeout limit to be applied unconditionally
  # ; even in such cases.
  # ; Default Value: no
  request_terminate_timeout_track_finished: 'no'
  # ; Set open file descriptor rlimit.
  # ; Default Value: system defined value
  rlimit_files: '1024'
  # Set max core size rlimit.
  # ; Possible Values: 'unlimited' or an integer greater or equal to 0
  # ; Default Value: system defined value
  rlimit_core: 'unlimited'
  # Clear environment in FPM workers
  # ; Prevents arbitrary environment variables from reaching FPM worker processes
  # ; by clearing the environment in workers before env vars specified in this
  # ; pool configuration are added.
  # ; Setting to "no" will make all environment variables available to PHP code
  # ; via getenv(), $_ENV and $_SERVER.
  # ; Default Value: yes
  clear_env: yes
  # ; Limits the extensions of the main script FPM will allow to parse. This can
  # ; prevent configuration mistakes on the web server side. You should only limit
  # ; FPM to .php extensions to prevent malicious users to use other extensions to
  # ; execute php code.
  # ; Note: set an empty value to allow all extensions.
  # ; Default Value: .php
  security_limit_extensions: '.php'
  # Pass environment variables like LD_LIBRARY_PATH. All $VARIABLEs are taken from
  # ; the current environment.
  environment_variables:
    hostname: ';env[HOSTNAME] = $HOSTNAME'
    path: ';env[PATH] = /usr/local/bin:/usr/bin:/bin'
    tmp: ';env[TMP] = /tmp'
    tmpdir: ';env[TMPDIR] = /tmp'
    temp: ';env[TEMP] = /tmp'
  # Defining 'extension' will load the corresponding shared extension from
  # ; extension_dir. Defining 'disable_functions' or 'disable_classes' will not
  # ; overwrite previously defined php.ini values, but will append the new value
  # ; instead.
  php_ini_directives:
    admin_value: 'php_admin_value[sendmail_path] = /usr/sbin/sendmail -t -i -f www@my.domain.com'
    flag: 'php_flag[display_errors] = off'
    error_log: 'php_admin_value[error_log] = /var/log/fpm-php.www.log'
    log_erros: 'php_admin_flag[log_errors] = on'
    memory_limit: 'php_admin_value[memory_limit] = 32M'
  ########################################################################
  #### php.ini
  ########################################################################
  ini_expose_php: "On"
  ini_memory_limit: "256M"
  ini_max_execution_time: "60"
  ini_max_input_time: "60"
  ini_max_input_vars: "1000"
  ini_realpath_cache_size: "32K"
  ini_file_uploads: "On"
  ini_upload_max_filesize: "64M"
  ini_max_file_uploads: "20"
  ini_post_max_size: "32M"
  ini_date_timezone: "America/Chicago"
  ini_allow_url_fopen: "On"
  ini_sendmail_path: "/usr/sbin/sendmail -t -i"
  ini_output_buffering: "4096"
  ini_short_open_tag: "Off"
  ini_disable_functions: []
  ini_session_cookie_lifetime: 0
  ini_session_gc_probability: 1
  ini_session_gc_divisor: 1000
  ini_session_gc_maxlifetime: 1440
  ini_session_save_handler: files
  ini_session_save_path: ''
  ini_error_reporting: "E_ALL & ~E_DEPRECATED & ~E_STRICT"
  ini_display_errors: "Off"
  ini_display_startup_errors: "Off"